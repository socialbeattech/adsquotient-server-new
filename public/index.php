<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
// use Slim\Factory\AppFactory;

error_reporting(0);
require_once 'functions.php';
// require_once __DIR__ . "/../../../adsquotient/client/Facebook/autoload.php";

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../../adsquotient/client/config.php';

$app = new \Slim\App();

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->get('/', function (Request $request, Response $response, $args) {
    $response->getBody()->write("Hey");
    return $response;
});

$app->post ('/user/register', function(Request $request, Response $response, $args) use ($mysqlLink) {
    $username = !empty($_REQUEST['username']) ? $_REQUEST['username'] : '';
    $password = !empty($_REQUEST['password']) ? $_REQUEST['password'] : '';
    if(empty($password) || empty($username)){
        $verification = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.'
        );
        $response->getBody()->write(json_encode($verification));
        return $response;
    }
    $sql = "SELECT id FROM clients WHERE username = '$username'";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){
        if(mysqli_stmt_execute($stmt)){
            mysqli_stmt_store_result($stmt);
            if(mysqli_stmt_num_rows($stmt) == 1){
                $verification = array(
                    'success' => false,
                    'message' => 'Username already exists.'
                );
                $response->getBody()->write(json_encode($verification));
                return $response;
            } else{
                $password = password_hash($password, PASSWORD_DEFAULT);
                $sql = "INSERT INTO clients (username, password) VALUES ('$username', '$password')";
                if($stmt = mysqli_prepare($mysqlLink, $sql)){
                    if(mysqli_stmt_execute($stmt)){
                        $verification = array(
                            'success' => true,
                            'message' => 'User registered successfully.',
                        );
                        $response->getBody()->write(json_encode($verification));
                        return $response;
                    } else{
                        $verification = array(
                            'success' => false,
                            'message' => 'Error in connecting with database.',
                        );
                        $response->getBody()->write(json_encode($verification));
                        return $response;
                    }
                }
            }
        } else {
            $verification = array(
                'success' => false,
                'message' => 'Error in connecting with database.',
            );
            $response->getBody()->write(json_encode($verification));
            return $response;
        }
    }
});

$app->post ('/user/login', function(Request $request, Response $response, $args) use ($mysqlLink) {
    $username = !empty($_REQUEST['username']) ? $_REQUEST['username'] : '';
    $password = !empty($_REQUEST['password']) ? $_REQUEST['password'] : '';
    if(empty($password) || empty($username)){
        $verification = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.'
        );
        $response->getBody()->write(json_encode($verification));
        return $response;
    }
    $sql = "SELECT id, username, password FROM clients WHERE username = '$username'";
    if($stmt = mysqli_prepare($mysqlLink, $sql)) {
        if(mysqli_stmt_execute($stmt)){
            mysqli_stmt_store_result($stmt);
            if(mysqli_stmt_num_rows($stmt) == 1){
                mysqli_stmt_bind_result($stmt, $user_id, $username, $hashed_password);
                if(mysqli_stmt_fetch($stmt)){
                    if(password_verify($password, $hashed_password)){
                        $fbsql = "SELECT fb_account_id FROM fb_details WHERE id_client = '$user_id'";
                        $result = mysqli_query($mysqlLink,$fbsql);
                        $row = mysqli_fetch_assoc($result);
                        $fb_link = NULL;
                        $gads_link = NULL;
                        if ($row['fb_account_id'] != NULL) {
                            $fb_link = $row['fb_account_id'];
                        }
                        $verification = array(
                            'success' => true,
                            'message' => 'Logged in successfully.',
                            'data' => array(
                                'name' => $username,
                                'user_id' => $user_id,
                                'user_email' => "",
                                'token' => "",
                                'integrations' => array("facebook" => $fb_link,"google" => $gads_link)
                            )
                        );
                        $response->getBody()->write(json_encode($verification));
                    } else {
                        $verification = array(
                            'success' => false,
                            'message' => 'Login details are incorrect.'
                        );
                        $response->getBody()->write(json_encode($verification));
                    }
                }
            } else {
                $verification = array(
                    'success' => false,
                    'message' => 'Username not found.',
                );
                $response->getBody()->write(json_encode($verification));
                return $response;
            }
        } else {
            $verification = array(
                'success' => false,
                'message' => 'Error in connecting with database.',
            );
            $response->getBody()->write(json_encode($verification));
            return $response;
        }
    }
    return $response;
});

$app->post ('/user/facebook/accountinfo', function(Request $request, Response $response, $args) use ($mysqlLink, $FB) {
    $access_token = !empty($_REQUEST['accesstoken']) ? $_REQUEST['accesstoken'] : '';
    if(empty($access_token)){
        $verification = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.'
        );
        $response->getBody()->write(json_encode($verification));
        return $response;
    }
    //adding access token to database
    $verification = array(
        'success' => true,
        'message' => 'Facebook account information successfully fetched.',
        'data' => $userData
    );
    $response->getBody()->write(json_encode($verification));
    return $response;
});

$app->get ('/datasets/get', function(Request $request, Response $response, $args) use ($mysqlLink) {
    $clientID = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $datasetArray = array();
    if(empty($clientID)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.',
            'data' => array()
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $sql = "SELECT * FROM campaigns WHERE client_id = '$clientID'";
    if($stmt = mysqli_prepare($mysqlLink, $sql)) {
        if($result = mysqli_query($mysqlLink, $sql)){
            if(mysqli_num_rows($result) > 0){
                while($row = mysqli_fetch_array($result)){
                    array_push($datasetArray, array(
                        'aqid' => $row['id'],
                        'aqcampname' => $row['aqcampname'],
                        'aqsector' => $row['aqsector'],
                        'aqcity' => $row['aqcity'],
                        'fb_ad_account' => $row['fb_ad_account'],
                    ));
                }
                $returnArray = array(
                    'success' => true,
                    'message' => count($datasetArray) . ' Record(s) found',
                    'data' => $datasetArray,
                );
                $response->getBody()->write(json_encode($returnArray));
                return $response;
            } else {
                $returnArray = array(
                    'success' => false,
                    'message' => 'No Record found',
                    'data' => array()
                );
                $response->getBody()->write(json_encode($returnArray));
                return $response;
            }
        } else {
            $returnArray = array(
                'success' => false,
                'message' => 'Error in connecting with database.',
                'data' => array()
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }
    }
});

$app->post ('/datasets/create', function(Request $request, Response $response, $args) use ($mysqlLink) {
    $clientID = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $sector = !empty($_REQUEST['sector']) ? $_REQUEST['sector'] : '';
    $campName = !empty($_REQUEST['campname']) ? $_REQUEST['campname'] : '';
    $campCity = !empty($_REQUEST['campcity']) ? $_REQUEST['campcity'] : '';
    $facebookCampaigns = !empty($_REQUEST['facebookcampaigns']) ? $_REQUEST['facebookcampaigns'] : '';
    $fbAdAccountName = !empty($_REQUEST['fbAdAccountName']) ? $_REQUEST['fbAdAccountName'] : '';
    $fbAdAccountId = !empty($_REQUEST['fbAdAccountId']) ? $_REQUEST['fbAdAccountId'] : '';
    // $googleCampaigns = !empty($_REQUEST['googlecampaigns']) ? $_REQUEST['googlecampaigns'] : '';
    if(empty($clientID) || empty($sector) || empty($campCity) || empty($campName)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $added_time = date("Y-m-d H:i:s");
    $sql = "INSERT INTO campaigns (client_id,aqcampname,aqsector,aqcity,added_time, campaign_id,fb_ad_account,fb_ad_account_name) VALUES ('$clientID', '$campName', '$sector', '$campCity', '$added_time', '$facebookCampaigns', '$fbAdAccountId', '$fbAdAccountName')";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){    
        if(mysqli_stmt_execute($stmt)){
            $lastInsertId = mysqli_insert_id($mysqlLink);
            $dataArray = array(
                'lastinsertid' => $lastInsertId
            );
            $returnArray = array(
                'success' => true,
                'message' => 'Dataset created successfully.',
                'data' => $dataArray
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        } else{
            $returnArray = array(
                'success' => false,
                'message' => 'Error in connecting with database.',
                'data' => array()
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }
    }
});

$app->post ('/datasets/update', function(Request $request, Response $response, $args) use ($mysqlLink) {
    $id = !empty($_REQUEST['aqid']) ? $_REQUEST['aqid'] : '';
    $sector = !empty($_REQUEST['sector']) ? $_REQUEST['sector'] : '';
    $campName = !empty($_REQUEST['campname']) ? $_REQUEST['campname'] : '';
    $campCity = !empty($_REQUEST['campcity']) ? $_REQUEST['campcity'] : '';
    $dataArray = array();
    if(empty($id)){
        $returnArray = array(
            'success' => false,
            'message' => 'AQ ID is missing.',
            'data' => array()
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $sql = "SELECT * FROM campaigns WHERE id = '$id'";
    if($stmt = mysqli_prepare($mysqlLink, $sql)) {
        if($result = mysqli_query($mysqlLink, $sql)){
            if(mysqli_num_rows($result) == 1){
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $sector = ($sector == '') ? $row['aqsector'] : $sector;
                $campName = ($campName == '') ? $row['aqcampname'] : $campName;
                $campCity = ($campCity == '') ? $row['aqcity'] : $campCity;
                $dataArray = array(
                    'id' => $id,
                    'aqcampname' => $campName,
                    'aqsector' => $sector,
                    'aqcity' => $campCity
                );
                $sql = "UPDATE `campaigns` SET `aqcampname` = '$campName', `aqsector` = '$sector', `aqcity` = '$campCity' WHERE id = '$id'";
                if(mysqli_query($mysqlLink, $sql)){
                    $returnArray = array(
                        'success' => true,
                        'message' => 'Dataset updated successfully.',
                        'data' => $dataArray
                    );
                    $response->getBody()->write(json_encode($returnArray));
                    return $response;
                }
                else{
                    $returnArray = array(
                        'success' => false,
                        'message' => 'Error in connecting with database.',
                        'data' => array()
                    );
                    $response->getBody()->write(json_encode($returnArray));
                    return $response;
                }
            } else {
                $returnArray = array(
                    'success' => false,
                    'message' => 'Invalid ID parameter.',
                    'data' => array()
                );
                $response->getBody()->write(json_encode($returnArray));
                return $response;
            }
        }
    } else {
        $returnArray = array(
            'success' => false,
            'message' => 'Error in connecting with database.',
            'data' => array()
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
});

$app->post ('/datasets/delete', function(Request $request, Response $response, $args) use ($mysqlLink) {
    $id = !empty($_REQUEST['aqid']) ? $_REQUEST['aqid'] : '';
    if(empty($id)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $sql = "DELETE FROM campaigns WHERE id = '$id'";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){
        if(mysqli_stmt_execute($stmt)){
            $returnArray = array(
                'success' => true,
                'message' => 'Dataset deleted successfully.',
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        } else {
            $returnArray = array(
                'success' => false,
                'message' => 'Error in connecting with database.',
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }
    }

    $sql = "SELECT * FROM campaigns WHERE id = '$id'";
    if($stmt = mysqli_prepare($mysqlLink, $sql)) {
        if($result = mysqli_query($mysqlLink, $sql)){
            if(mysqli_num_rows($result) == 1){
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $dataArray = array(
                    'id' => $row['id'],
                    'aqcampname' => $row['aqcampname'],
                    'aqsector' => $row['aqsector'],
                    'aqcity' => $row['aqcity'],
                    'campIds' => $row['campaign_id']
                );
                $returnArray = array(
                    'success' => true,
                    'message' => 'Client data found.',
                    'data' => $dataArray
                );
                $response->getBody()->write(json_encode($returnArray));
                return $response;
            } else {
                $returnArray = array(
                    'success' => false,
                    'message' => 'Invalid ID parameter.',
                    'data' => array()
                );
                $response->getBody()->write(json_encode($returnArray));
                return $response;
            }
        }
    } else {
        $returnArray = array(
            'success' => false,
            'message' => 'Error in connecting with database.',
            'data' => array()
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
});

$app->post ('/accounts/facebook/get', function(Request $request, Response $response, $args) use ($mysqlLink, $FB) {
    $clientID = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
    if(empty($clientID)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $access_token = doGetFbAccessTokenFromCampaignId($mysqlLink, $clientID);
    if(empty($access_token)){
        $returnArray = array(
            'success' => false,
            'message' => 'Error in retrieving access token.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    try {
        $FBresponse = $FB->get("/me?fields=name,email,picture{url},adaccounts.limit(300){id,account_id,name,amount_spent,currency,created_time,account_status}", $access_token);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        $returnArray = array(
            'success' => false,
            'message' => 'Graph returned an error: ' . $e->getMessage(),
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    } catch(Facebook\Exceptions\FacebookSDKException $e) { 
        $returnArray = array(
            'success' => false,
            'message' => 'Facebook SDK returned an error: ' . $e->getMessage(),
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    } catch (Exception $e){
        $returnArray = array(
            'success' => false,
            'message' => 'Ads Quotient exception: ' . $e->getMessage(),
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    if($FBresponse){
        $userData = $FBresponse->getGraphNode()->asArray();
        $returnArray = array(
            'success' => true,
            'message' => 'FB Account details retrieved successfully.',
            'data' => $userData
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
});

$app->post ('/accounts/facebook/update', function(Request $request, Response $response, $args) use ($mysqlLink, $FB) {
    $aqID = !empty($_REQUEST['aqid']) ? $_REQUEST['aqid'] : '';
    $acc_id = !empty($_REQUEST['acc_id']) ? $_REQUEST['acc_id'] : '';
    $adname = !empty($_REQUEST['adname']) ? $_REQUEST['adname'] : '';
    if(empty($aqID) || empty($acc_id) || empty($adname)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $sql = "UPDATE campaigns SET `fb_ad_account` = '$acc_id', `fb_ad_account_name` = '$adname', `campaign_id` = '' WHERE `id`= '$aqID'; ";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){
        if(mysqli_stmt_execute($stmt)){
            $returnArray = array(
                'success' => true,
                'message' => 'AQ Campaign created successfully.',
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        } else {
            $returnArray = array(
                'success' => false,
                'message' => 'Error in connecting with database.',
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }
    }
});

$app->post ('/campaigns/facebook/get', function(Request $request, Response $response, $args) use ($mysqlLink, $FB) {
    $clientID = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $fbAccountID = !empty($_REQUEST['fbaccountid']) ? $_REQUEST['fbaccountid'] : '';
    if(empty($clientID) || empty($fbAccountID)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $dateRange = !empty($_REQUEST["date_filter"]) ? $_REQUEST["date_filter"] : "lifetime";
    // To get fb access token and account id
    $access_token = doGetFbAccessTokenFromCampaignId($mysqlLink, $clientID);
    //To collect the campaign IDS from database
    $aqCampResultAry = doGetCampIdFromAQId($mysqlLink, $aqID, $clientID);
    $dbAqCampName =  !empty($aqCampResultAry['data']['aqcampname']) ? $aqCampResultAry['data']['aqcampname'] : '';
    $dbAqFbAdAccountName =  !empty($aqCampResultAry['data']['aqAdAccount']) ? $aqCampResultAry['data']['aqAdAccount'] : '';
    if(!empty($aqCampResultAry['data']['campaign_id'])){
        $dbCampId = $aqCampResultAry['data']['campaign_id'];
    }
    if(!empty($access_token) && !empty($fbAccountID)){
        $fbParameters = $fbAccountID."/campaigns?fields=name,objective,id,status,insights.date_preset(".$dateRange."){ad_name,campaign_name,objective,spend}&limit=350";
        try {
            $FBresponse = $FB->get($fbParameters, $access_token);
        } catch (\Exception $e) {
            $returnArray = array(
                'success' => false,
                'message' => $e->getMessage(),
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }
        
        if(!empty($FBresponse)){
            $campaignListData = $FBresponse->getGraphEdge()->asArray();
            $returnArray = array(
                'success' => true,
                'message' => 'Campaign list successfully retrieved.',
                'data' => array(
                    'aqcampname' => $dbAqCampName,
                    'aqfbadaccountname' => $dbAqFbAdAccountName,
                    'campaignlistdata' => $campaignListData,
                    'selectedcampaigns' => $dbCampId
                )
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        } else {
            $returnArray = array(
                'success' => false,
                'message' => 'No campaigns found.',
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }   
    } else {
        $returnArray = array(
            'success' => false,
            'message' => 'No access token or account ID found.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
});

$app->post ('/campaigns/facebook/update', function(Request $request, Response $response, $args) use ($mysqlLink, $FB) {
    $aqID = !empty($_REQUEST['aqid']) ? $_REQUEST['aqid'] : '';
    $clientID = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $campaignsId = !empty($_REQUEST['campaignsid']) ? $_REQUEST['campaignsid'] : '';
    if(empty($clientID) || empty($aqID)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are missing.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $dbCampId = implode(',',$campaignsId);
    $sql = "UPDATE campaigns SET campaign_id = '$dbCampId' WHERE id = $aqID";
    if($stmt = mysqli_prepare($mysqlLink, $sql)){
        if(mysqli_stmt_execute($stmt)){
            $sql = "DELETE FROM all_campaign_data WHERE `aqID` = '$aqID'";
            $query = mysqli_query($mysqlLink, $sql);

            $access_token = doGetFbAccessTokenFromCampaignId($mysqlLink, $aqID);
            if(empty($access_token)){
                $returnArray = array(
                    'success' => false,
                    'message' => 'Error in retrieving access token.',
                );
                $response->getBody()->write(json_encode($returnArray));
                return $response;
            }
            doFetchCampaignsData($mysqlLink, $aqID, $clientID, $FB, $access_token);
            $returnArray = array(
                'success' => true,
                'message' => 'Campaign List updated successfully.',
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        } else{
            $returnArray = array(
                'success' => false,
                'message' => 'Error in connecting with database.',
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }
    }
});

$app->post ('/data/compare/facebook', function(Request $request, Response $response, $args) use ($mysqlLink, $FB) {
    $aqID = !empty($_REQUEST['aqid']) ? $_REQUEST['aqid'] : '';
    $clientID = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $aqDuration = !empty($_REQUEST['aqduration']) ? $_REQUEST['aqduration'] : 'lifetime';
    $fbAdAccountName = '';
    $access_token = '';
    if(empty($aqID) || empty($clientID) || empty($aqDuration)){
        $returnArray = array(
            'success' => false,
            'message' => 'Parameter(s) are  missing.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $access_token = doGetFbAccessTokenFromCampaignId($mysqlLink, $clientID);
    if(empty($access_token)){
        $returnArray = array(
            'success' => false,
            'message' => 'Error in retrieving access token.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    $fbDatasetDetails = doGetFbAdAccountNameCampaignId($mysqlLink, $aqID);
    if(empty($fbDatasetDetails)){
        $returnArray = array(
            'success' => false,
            'message' => 'Error in retrieving Ad Account Name.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }
    if(is_nan($aqID)){
        $returnArray = array(
            'success' => false,
            'message' => 'Not a valid Ads Quotient ID.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }

    $dataAry = array();
    $today = date('Y-m-d');
    
    $aqCampResultAry = doGetCampIdFromAQId($mysqlLink, $aqID, $clientID);
    if($aqCampResultAry['success']){
        $dbaqCity = !empty($aqCampResultAry['data']['aqcity']) ? $aqCampResultAry['data']['aqcity'] : 'City not available';
        $dbInsights = !empty($aqCampResultAry['data']['aqinsights']) ? $aqCampResultAry['data']['aqinsights'] : 'Insights not available';
        $dbAqCampName = !empty($aqCampResultAry['data']['aqcampname']) ? $aqCampResultAry['data']['aqcampname'] : '';
        $dbAqFbAdAccountName  = !empty($aqCampResultAry['data']["aqAdAccount"]) ? $aqCampResultAry['data']["aqAdAccount"] : '';
        $dbaqSector = !empty($aqCampResultAry['data']['aqsector']) ? $aqCampResultAry['data']['aqsector'] : 'Sector not available';
        if(!empty($aqCampResultAry['data']['campaign_id'])){
            $dbCampIdAry = explode(',',$aqCampResultAry['data']['campaign_id']);
        }
    }

    $aqMetric = 'all';
    $aqDuration = 'lifetime';
    $aqCampSector = $dbaqSector;
    $aqCity = $dbaqCity;

    // so filter will work with corresponding cities else it will fetch all cities
    $cityBasedSearch = array('real_estate_lessthan_50l','real_estate_50l_1c','real_estate_1c_2c','real_estate_greaterthan_2c');

    // printArray($aqCampSector);
    if(!empty($aqCampSector) && !empty($aqCity)){
        $qryWhere = "";
        if(in_array($aqCampSector,$cityBasedSearch)){
            $qryWhere = " AND  `city` like ? ";
        }else{
            $qryWhere = " AND  (`city` like ? OR `city` like 'ALL') ";
        }
        // Prepare a select statement
        $sql = "SELECT * FROM benchmark_details WHERE `aqsector` LIKE ? $qryWhere ; ";
        if($stmt = mysqli_prepare($mysqlLink, $sql)){
            mysqli_stmt_bind_param($stmt, "ss", $aqCampSector, $aqCity);
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
                if(mysqli_num_rows($result) >= 1){
                    while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                        $row['objective'] = strtoupper($row['objective']);
                        $benchMarkDetailsData[] = $row;
                    }
                } else{
                    $returnArray = array(
                        'success' => false,
                        'message' => 'Benchmark details not available for the following data.',
                    );
                    $response->getBody()->write(json_encode($returnArray));
                    return $response;
                }
            } else{
                $returnArray = array(
                    'success' => false,
                    'message' => 'Error in connecting with database.',
                );
                $response->getBody()->write(json_encode($returnArray));
                return $response;
            }
        }
    }
    if(!empty($benchMarkDetailsData)){
        foreach ($benchMarkDetailsData as $benchMarkData) {
            $benchMarkDetailsAry[$benchMarkData['objective']] = array();
            foreach ($benchMarkData as $key => $value) {
                $benchMarkDetailsAry[$benchMarkData['objective']][$key] = $value;
            }
        }
    }else{
        $returnArray = array(
            'success' => false,
            'message' => 'Benchmark details not available for the following data.',
        );
        $response->getBody()->write(json_encode($returnArray));
        return $response;
    }

    $averageAry = array();
    // Total number of campaigns selected to compare
    $totalCamp = count($dbCampIdAry);
  
    if(!empty($dbCampIdAry)){
        $eachBatch = 10;
        $numberOfBatches = (($totalCamp%$eachBatch) == 0)?(int)($totalCamp/$eachBatch) : (int)($totalCamp/$eachBatch)+1;
        $overAllCamp = array();
        for($i = 0; $i < $numberOfBatches; $i++){
        $currentBatch = array();
        $compareJWith = ($i==0) ? $eachBatch : ( ( ($eachBatch * $i) - 1 ) + $eachBatch);
        for($j = ($i==0) ? 0 : ( ($eachBatch * $i) - 1 ); $j < $compareJWith; $j++)
        {
            if(!$dbCampIdAry[$j])
                break;
            $fbParameters = $dbCampIdAry[$j]."/insights?fields=campaign_name,campaign_id,objective,spend,cpc,ctr,cpp,cpm,cost_per_action_type,cost_per_conversion,cost_per_thruplay&action_attribution_windows=['28d_click','28d_view']&date_preset=".$aqDuration;
            $request = $FB->request('GET', $fbParameters);
            $currentBatch["$j"] = $request;
        }
        try {
            $FBresponse = $FB->sendBatchRequest($currentBatch, $access_token);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $returnArray = array(
                'success' => false,
                'message' => $e->getMessage(),
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $returnArray = array(
                'success' => false,
                'message' => $e->getMessage(),
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        }
      
        foreach($FBresponse as $data){
            $countOfCampaignRequest++;
            if(!empty($data)){
                $errorCheck = $data->getHeaders();
                $errorCheck = reset(json_decode($errorCheck['X-Business-Use-Case-Usage']))[0];
                if($countOfCampaignRequest == 1 || $countOfCampaignRequest == $totalCamp){
                    $facebookRequestLimit = "Call count = ".$errorCheck->call_count."' + ' Total CPU Time = ".$errorCheck->total_cputime."' + ' Total Time = ".$errorCheck->total_time." Estimated time to regain access = ".$errorCheck->estimated_time_to_regain_access;
                }
                if($errorCheck->total_time > 80 || $errorCheck->total_cputime > 80)
                {
                    $returnArray = array(
                        'success' => false,
                        'message' => 'Facebook Ads Exception: Too many requests have been made from the account. Please try again after one hour.',
                    );
                    $response->getBody()->write(json_encode($returnArray));
                    return $response;
                }
                $accDataAry[] = $data->getGraphEdge()->asArray();
            }
        }
    }
    
    $metrics = array('cpc', 'b_cpc', 'cpm', 'b_cpm', 'cpl', 'b_cpl', 'ctr', 'b_ctr', 'cptp', 'b_cptp', 'cpe', 'b_cpe');
    $count = 0;
    if(!empty($accDataAry)){
        // $fbDataResultAry = doGetCampDetails1($accDataAry);
        $fbCampNameAry  = doGetFBCampName($accDataAry);
        $sql = "DELETE FROM all_campaign_data WHERE `aqID` = '$aqID'";
        $query = mysqli_query($mysqlLink, $sql);
        // foreach($fbDataResultAry as $fbDataResult){
            // $json = json_encode($fbDataResult);
            // $benchmarkDetailsJson = json_encode($benchMarkDetailsAry);
            // $campaignNamesJson = json_encode($fbCampNameAry);
            foreach($accDataAry as $campaignData){
                $currentDataInsights = array();
                $currentFBArray = array();
                $fbObjective = !empty($campaignData[0]['objective']) ? $campaignData[0]['objective'] : '';
                $fbCPC = $fbCPM = $fbCPL = $fbCTR = $fbCPTP = $fbCPV = $fbCPE = $fbAppInstall = '';
                $fbCPC = doGetCPC($campaignData,$fbObjective);
                $fbCPM = doGetCPM($campaignData);
                $fbCPL = doGetCPL($campaignData);
                $fbCTR = doGetCTR($campaignData);
                $fbCPTP = doGetCPTP($campaignData);
                $fbCPV = doGetCPV($campaignData);
                $fbCPE = doGetCPE($campaignData);
                $fbAppInstall = doGetAppInstall($campaignData);
                $fbCampName = $campaignData[0]['campaign_name'];
                $fbCampId = $campaignData[0]['campaign_id'];
                $currentFBArray = array('campaign_name'=>$fbCampName,'campaign_id'=>$fbCampId,'objective'=>$fbObjective,'cpc'=>$fbCPC,'cpm'=>$fbCPM,'cpl'=>$fbCPL,'ctr'=>$fbCTR,'cptp'=>$fbCPTP,'cpv'=>$fbCPV,'cpe'=>$fbCPE,'cpi'=>$fbAppInstall, 'b_cpc'=>$benchMarkDetailsAry[$fbObjective]['fb_cpc'],'b_cpm'=>$benchMarkDetailsAry[$fbObjective]['fb_CPM'],'b_cpl'=>$benchMarkDetailsAry[$fbObjective]['fb_CPL'],'b_ctr'=>$benchMarkDetailsAry[$fbObjective]['fb_CTR'],'b_cptp'=>$benchMarkDetailsAry[$fbObjective]['fb_CPTP'],'b_cpv'=>$benchMarkDetailsAry[$fbObjective]['fb_CPV'],'b_cpe'=>$benchMarkDetailsAry[$fbObjective]['fb_CPE'],'b_cpi'=>$benchMarkDetailsAry[$fbObjective]['fb_CPI']);
                $fbAry[] = $currentFBArray;
                $currentDataInsights = insights($mysqlLink, $currentFBArray, $metrics);
                // if(!empty($currentDataInsights))
                // $count++;
                $insights[] = array('objective' => $fbObjective, 'insights' => $currentDataInsights);
            }
            $returnArray = array(
                'success' => true,
                'message' => 'Campaign Data fetched successfully.',
                'data' => array(
                    'fbDatasetDetails' => $fbDatasetDetails,
                    'campaignData' => $fbAry,
                    'benchmarkData' => $benchMarkDetailsAry,
                    'campaignNames' => $fbCampNameAry,
                    'insights' => $insights
                )
            );
            $response->getBody()->write(json_encode($returnArray));
            return $response;
        // }
    }
    }else{
    $returnArray = array(
        'success' => false,
        'message' => 'Error in fetching campaign details',
    );
    $response->getBody()->write(json_encode($returnArray));
    return $response;
    }
});

// Catch-all route to serve a 404 Not Found page if none of the routes match
// NOTE: make sure this route is defined last
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

$app->run();